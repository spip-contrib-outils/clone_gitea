#!/bin/bash
# 
# script de synchro du gitea de SPIP vers le serveur de sauvegarde (clone)
# lance par le compte git du serveur de prod
# (sa cle id_rsa.pub est OK sur le clone sans mot de passe)
#
# pour simplifier le clone a meme user/pass/bdd que le gitea maitre
#

## CONFIG
DATE=$(date +%Y-%m-%d_%H-%M)

# recuperer le fichier de config externe
source ./clone_gitea.conf


## GO GO GO

# virer les sauvegardes les plus vieilles
BACKUPS=`find $CHEMIN_DUMP -name "dump_gitea_*.sql.bzip" | wc -l | sed 's/\ //g'`
while [ $BACKUPS -ge $A_GARDER ]; do
  ls -tr1 $CHEMIN_DUMP/dump_gitea_*.sql.bzip | head -n 1 | xargs rm -f 
  BACKUPS=`expr $BACKUPS - 1` 
done

# dump de la bdd
mysqldump -u $USER_MYSQL --password=$PASS_MYSQL -B --opt $DB_MYSQL | bzip2 -c -1 > $CHEMIN_DUMP/$FICHIER_DUMP

# envoi du dump
rsync -azv -e "ssh -p $PORT_CLONE -i $CLE_CLONE" $CHEMIN_DUMP/*.bzip $USER_CLONE@$SERVEUR_CLONE:$CHEMIN_CLONE/dump/

# synchro des fichiers attaches
rsync -azv -e "ssh -p $PORT_CLONE -i $CLE_CLONE" $CHEMIN_DATA/attachments $USER_CLONE@$SERVEUR_CLONE:$DATA_CLONE/
rsync -azv -e "ssh -p $PORT_CLONE -i $CLE_CLONE" $CHEMIN_DATA/avatars $USER_CLONE@$SERVEUR_CLONE:$DATA_CLONE/
rsync -azv -e "ssh -p $PORT_CLONE -i $CLE_CLONE" $CHEMIN_DATA/repo-avatars $USER_CLONE@$SERVEUR_CLONE:$DATA_CLONE/

# synchro des repos 
# on utilise -rlpt à la place de -a pour ne pas synchro proprio et groupe 
# puisque le proprio des repos sur prod n'est pas forcement identique a celui du clone
rsync -rlptzv -e "ssh -p $PORT_CLONE -i $CLE_CLONE" $CHEMIN_REPOS/* $USER_CLONE@$SERVEUR_CLONE:$REPOS_CLONE/

# injecter le dump dans la BDD du clone 
echo "Depart injection dump sur le clone"
echo 'cmde:  ssh -p $PORT_CLONE -i ' $CLE_CLONE $USER_CLONE@$SERVEUR_CLONE "bunzip2 -c $CHEMIN_CLONE/dump/$FICHIER_DUMP | mysql -u$USER_MYSQL --password=$PASS_MYSQL -D $DB_MYSQL"

ssh -p $PORT_CLONE -i $CLE_CLONE $USER_CLONE@$SERVEUR_CLONE "bunzip2 -c $CHEMIN_CLONE/dump/$FICHIER_DUMP | mysql -u$USER_MYSQL --password=$PASS_MYSQL -D $DB_MYSQL"

